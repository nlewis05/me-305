'''
@file Elevator.py

Source Code:https://bitbucket.org/nlewis05/me-305/src/master/HW%202/Elevator.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator.

The user has a button to go to the first floor and another to go to the second floor.

There are sensors on both floors.
'''

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of the elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0

    ## Constant defining State 1
    S1_MOVING_DOWN      = 1

    ## Constant defining State 2
    S2_MOVING_UP        = 2

    ## Constant defining State 3
    S3_STOPPED_ON_FLOOR1      = 3

    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR2      = 4

    
    def __init__(self):
        '''
        @brief      Creates a TaskElevator object.
        '''
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        
        
    
    def run(self, button_1, button_2, first, second):
        '''
        @brief      Runs one iteration of the task
        '''
        
        ## A class attribute "copy" of the GoButton object
        self.button_1 = button_1
        
        ## The button object used for the left limit
        self.button_2 = button_2
        
        ## The button object used for the right limit
        self.first = first
        
        self.second = second
        
        # 0 to 3
        if(self.state == self.S0_INIT):
            # Run State 0 Code
            self.transitionTo(self.S3_STOPPED_ON_FLOOR1)
            self.Motor = 0
            self.first = 1
            self.second = 0
            self.button_1 = 0
            self.button_2 = 0
        # 3 to 2
        elif(self.state == self.S3_STOPPED_ON_FLOOR1):
            if self.button_1 == 0 and button_2 == 1 and second == 0:
                self.transitionTo(self.S2_MOVING_UP)
                self.Motor = 1
                self.first = 0
                self.second = 0
                self.button_1 = 0
                self.button_2 = 1
        # 2 to 4        
        elif(self.state == self.S2_MOVING_UP):
            # Run State 2 Code
            if self.button_1 == 0 and button_2 == 1 and second == 1:
                self.transitionTo(self.S4_STOPPED_ON_FLOOR2)
                self.Motor = 0
                self.first = 0
                self.second = 1
                self.button_1 = 0
                self.button_2 = 0
        # 4 to 1    
        elif(self.state == self.S4_STOPPED_ON_FLOOR2):
            if self.button_1 == 1 and button_2 == 0 and first == 0:
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor = 2
                self.first = 0
                self.second = 0
                self.button_1 = 1
                self.button_2 = 0
       # 1 to 3 
        elif(self.state == self.S1_MOVING_DOWN):
            if self.button_1 == 1 and button_2 == 0 and first == 1:
                self.transitionTo(self.S3_STOPPED_ON_FLOOR1)
                self.Motor = 0
                self.first = 1
                self.second = 0
                self.button_1 = 0
                self.button_2 = 0
        
        else:
            # Uh-oh state (undefined sate)
            # Error handling
            pass
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
if __name__== '__main__':
    Elevator = TaskElevator()
    print(Elevator.state)
    Elevator.run(0, 0, 1, 0) # 4 to 1
    print(Elevator.state)
    Elevator.run(Elevator.button_1,1, Elevator.first, Elevator.second)
    print(Elevator.state)
    Elevator.run(Elevator.button_1,Elevator.button_2, Elevator.first, 1)
    print(Elevator.state)