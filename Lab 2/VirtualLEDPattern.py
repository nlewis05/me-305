## @file VirtualLEDPattern.py
#  Source Code: https://bitbucket.org/nlewis05/me-305/src/master/Lab%202/VirtualLEDPattern.py?mode=edit&spa=0&at=master&fileviewer=file-view-default
#  This file uses a finite state machine that turns a virtual LED on and off.

import time

class TaskVirtualLEDPattern:
    '''
    @brief      A finite state machine to control a virtual LED.
    @details    This class implements a finite state machine to control the
                operation of the elevator.
    '''
    S1_LED_ON = 1
    
    S2_LED_OFF = 2
    
    def __init__(self, interval):
        '''
        @brief      Creates a TaskVirtualLEDPattern object.
        '''
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S1_LED_ON
        
#        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval      
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
        
      
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
         # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S1_LED_ON):
                self.transitionTo(self.S2_LED_OFF)
                print("LED OFF")
                
            elif(self.state == self.S2_LED_OFF):
                self.transitionTo(self.S1_LED_ON)
                print("LED ON")
                
            self.next_time += self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
# if __name__== '__main__':
#     task1 = TaskVirtualLEDPattern(1)
#     for N in range(100000):
#         task1.run()