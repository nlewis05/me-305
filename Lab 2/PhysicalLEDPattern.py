# -*- coding: utf-8 -*-
"""
@file PhysicalLEDPattern.py

Source Code: https://bitbucket.org/nlewis05/me-305/src/4c656748af1ad341906357bf1d18065edc4aa488/Lab%202/PhysicalLEDPattern.py?embed=t

This file uses a finite state machine that changes the brightness of an LED in a sinusoidal pattern.

"""
import time
import math
import pyb

class TaskPhysicalLEDPattern:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                brightness of the LED.
    '''
    S1_LED_ON = 1
    
    S2_LED_ON = 2
    
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    def __init__(self, interval):
        '''
        @brief      Creates a TaskPhysicalLEDPattern object.
        '''
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S1_LED_ON
        
        #self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval      
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
        
      
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
          # checking if the timestamp has exceeded our "scheduled" timestamp
        
        if (self.state == self.S1_LED_ON):
            if (self.curr_time >= self.next_time):
                  percent=50*math.sin((math.pi/5)*(self.curr_time-self.start_time))+50
                  pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
                  tim2 = pyb.Timer(2, freq = 20000)
                  t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
                  t2ch1.pulse_width_percent(percent)
                  self.next_time += self.interval
                 
        else:
            pass
        
# if __name__== '__main__':
#     task1 = TaskPhysicalLEDPattern(.1)
#     for N in range(100000):
#         task1.run()