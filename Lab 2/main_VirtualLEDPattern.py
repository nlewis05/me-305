'''
@file main_VirtualLEDPattern.py
'''
from VirtualLEDPattern import TaskVirtualLEDPattern
        
## Task object
task1 = TaskVirtualLEDPattern(1)

# To run the task call task1.run() over and over
for N in range(10000000):
    task1.run()
#    task2.run()
#    task3.run()
