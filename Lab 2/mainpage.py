## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Some information about the whole project
#
#  @section sec_ele Elevator
#  In this assignment, I created a class that simulates the behavior of an Elevator. 
#  @image html StateDiagram.PNG
#  State Diagram: https://bitbucket.org/nlewis05/me-305/src/master/HW%202/State%20Diagram.PNG
#
#  @section sec_phy Physical LED Pattern
#  In this assignment, I created a finite state machine to the change the brightness of an LED in a sinusoidal pattern.
#
#  @section sec_vir Virtual LED Pattern
#  In this assignment, I created a finite state machine to turn a virtual LED on and off. 
#
#  @author Natalie Lewis