# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 00:35:58 2020

@author: Administrator
"""

import pyb
import math

pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
curr_time=5
start_time=2.5
percent=50*math.sin((math.pi/5)*(curr_time-start_time))+50
t2ch1.pulse_width_percent(percent)