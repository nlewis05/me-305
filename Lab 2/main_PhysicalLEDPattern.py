'''
@file main_PhysicalLEDPattern.py
'''
from PhysicalLEDPattern import TaskPhysicalLEDPattern
        
## Task object
task1 = TaskPhysicalLEDPattern(.1)

# To run the task call task1.run() over and over
for N in range(100000):
    task1.run()
#    task2.run()
#    task3.run()
